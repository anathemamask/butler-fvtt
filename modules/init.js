import constants from "./constants.js";
import registerSettings from "./settings.js";
import ButlerOverlay from "./apps/ButlerOverlay.js";

Hooks.once('init', () => {
  registerSettings();
  //CONFIG.debug.hooks = true --- Definitely didn't leave this on in initial release. I swear.
  Hooks.callAll(`${constants.moduleName}:afterInit`);
});

  
Hooks.on("canvasReady", () => {
  if (game.settings.get("butler", 'hide-all') === false) {
  ButlerOverlay.init();
  
    
    if (ui.butlerOverlay) ui.butlerOverlay.render();
  Hooks.callAll(`${constants.moduleName}:afterCanvasReady`);
  }
});

Hooks.on("renderSceneControls", () => {
  if (game.settings.get("butler", 'hide-all') === false) {
   
  $("#token-menu-help").hide()
  $("#template-menu-help").hide()
  $("#notes-menu-help").hide()
  if (game.settings.get("butler", 'hide-toolbar-help') === false) {
    if ($("#navigation").outerHeight() <= 42) {
      if ($(".scene-control").attr('data-control') === "token") {
        if (ui.controls.activeControl === "token") {
          $("#token-menu-help").delay(300).fadeIn("slow")
        }
      }
    if (ui.controls.activeControl === "measure") {
      $("#template-menu-help").delay(300).fadeIn("slow")
    }
    if (ui.controls.activeControl === "notes") {
      $("#notes-menu-help").delay(300).fadeIn("slow")
    }
    } 
  }
}
})

Hooks.on("controlToken", (token, controlled) => {
  if (game.settings.get("butler", 'hide-all') === false) {
    if (game.settings.get("butler", 'hide-token-help-frame') === false) {
  if (controlled) {
    let butlerToken = canvas.tokens.controlled[0]
    let tokenFrame = $("#token-help-frame")
    let tX = butlerToken._validPosition.x -200
    let tY = butlerToken._validPosition.y -100
    let frameTop = `${tY}px`
    let frameLeft = `${tX}px`
    tokenFrame.detach()
     $("#hud").append(tokenFrame)
      tokenFrame.css({ 'top': `${frameTop}`, 'left': `${frameLeft}`, 'width': 600+'px'});
      if (game.settings.get("butler", 'hide-token-move-help') === true) {
        $(".token-help-left").hide() 
      }
      if (game.settings.get("butler", 'hide-token-hud-help') === true) {
        $(".token-help-right").hide() 
      }
      if (game.settings.get("butler", 'hide-token-click-help') === true) {
        $("#token-help-center").hide() 
      }
      tokenFrame.fadeIn("slow")

  } else {
    let tokenFrame = $("#token-help-frame")
    console.log("Token was deselected");
    $("#token-menu-help").hide()
    $("#template-menu-help").hide()
    $("#notes-menu-help").hide()
    tokenFrame.hide()

  }
}
}
})

Hooks.on("renderSettings", (token, controlled) => {
  if (game.settings.get("butler", 'hide-all') === false) {
    if (game.settings.get("butler", 'hide-token-help-frame') === false) {
  if (canvas.tokens.controlled.length != 0||null||undefined) {
    let tokenFrame = $("#token-help-frame")
    tokenFrame.hide()
    if (controlled) {
      if (canvas.tokens.controlled[0]){
      let butlerToken = canvas.tokens.controlled[0]
      let tX = butlerToken._validPosition.x -200
      let tY = butlerToken._validPosition.y -100
      
        let frameTop = `${tY}px`
        let frameLeft = `${tX}px`
        
        
       tokenFrame.detach()
       $("#hud").append(tokenFrame)
        tokenFrame.css({ 'top': `${frameTop}`, 'left': `${frameLeft}`, 'width': 600+'px'});
        if (game.settings.get("butler", 'hide-token-move-help') === true) {
          $(".token-help-left").hide() 
        }
        if (game.settings.get("butler", 'hide-token-hud-help') === true) {
          $(".token-help-right").hide() 
        }
        if (game.settings.get("butler", 'hide-token-click-help') === true) {
          $("#token-help-center").hide() 
          
        }
        
        tokenFrame.fadeIn("slow")
        
      }
      if (game.settings.get("butler", 'hide-toolbar-help') === false) {
     if ($(".scene-control").attr('data-control') === "token" && $("#navigation").outerHeight() >= 42) {

      if (ui.controls.activeControl === "token") {
        $("#token-menu-help").delay(300).fadeIn("slow")
      }
    }
  }
    } else {
      console.log("Token was deselected");
      tokenFrame.hide()
      $("#token-menu-help").hide()
      $("#template-menu-help").hide()
      $("#notes-menu-help").hide()
    } 
  }
}
}
})

Hooks.on("getSceneControlButtons", () => {
  $("#token-menu-help").hide()
  $("#template-menu-help").hide()
  $("#notes-menu-help").hide()
})
Hooks.on("updateToken", (token, controlled) => {
  if (game.settings.get("butler", 'hide-all') === false) {
    if (game.settings.get("butler", 'hide-token-help-frame') === false) {
  let tokenFrame = $("#token-help-frame")
  tokenFrame.hide()
  if (controlled) {
    if (canvas.tokens.controlled[0]){
    let butlerToken = canvas.tokens.controlled[0]
    let tX = butlerToken._validPosition.x -200
    let tY = butlerToken._validPosition.y -100
      let frameTop = `${tY}px`
      let frameLeft = `${tX}px`
     tokenFrame.detach()
     $("#hud").append(tokenFrame)
      tokenFrame.css({ 'top': `${frameTop}`, 'left': `${frameLeft}`, 'width': 600+'px'});
      if (game.settings.get("butler", 'hide-token-move-help') === true) {
        $(".token-help-left").hide() 
      }
      if (game.settings.get("butler", 'hide-token-hud-help') === true) {
        $(".token-help-right").hide() 
      }
      if (game.settings.get("butler", 'hide-token-click-help') === true) {
        $("#token-help-center").hide() 
      }
      tokenFrame.show()
  }
  } else {
    
    tokenFrame.hide()
    $("#token-menu-help").hide()
    $("#template-menu-help").hide()
    $("#notes-menu-help").hide()
  }
}
}
})

Hooks.on("canvasPan", () => {
  if (game.settings.get("butler", 'hide-all') === false) {
    
  if ($("#navigation").outerHeight() > 42) {
    $("#token-menu-help").hide()
    $("#template-menu-help").hide()
    $("#notes-menu-help").hide()
  }
  else if ($("#navigation").outerHeight() <= 42 ) {
    if (game.settings.get("butler", 'hide-toolbar-help') === false) {
    if (ui.controls.activeControl === "token") {
      $("#token-menu-help").delay(300).fadeIn("slow")
    }
    if (ui.controls.activeControl === "measure") {
      $("#template-menu-help").delay(300).fadeIn("slow")
    }
    if (ui.controls.activeControl === "notes") {
      $("#notes-menu-help").delay(300).fadeIn("slow")
    }
  }
    }
}
})
