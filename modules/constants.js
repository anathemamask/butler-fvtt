let constants = {
  modulePath: "modules/butler",
  moduleName: "butler",
  moduleLabel: "Butler UX Suite"
};
export default constants;
