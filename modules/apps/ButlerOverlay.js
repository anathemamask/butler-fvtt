import constants from "../constants.js";
import OverlayApplication from "./OverlayApplication.js";

export default class ButlerOverlay extends OverlayApplication {
  static app;
  tokenColors = {};
  positionSetting = 'unit-frame-box-position';

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: "butler-overlay",
      template: `${constants.modulePath}/templates/hud/butler-frame.html`,
      popOut: false
    });
  }
  
  getData(options = {}) {
    options = super.getData(options);
    return options;
  }

  static init() {
    const instance = new this();
    ui.butlerOverlay = instance;
   
    instance.render(true);
    
  }

  activateListeners(html) {
    
    super.activateListeners(html);
    const tokenmenubtn = $(`#controls > li:nth-child(1)`)
    const templatemenubtn = $(`#controls > li:nth-child(2)`)
    const macrobarHelp = $("#macro-bar-help")
    const chatboxHelp = $("#chat-box-help")
    const sidebartabHelp = $("#sidebar-tabs-help")
    const tokenmenuHelp = $("#token-menu-help")
    const templatemenuHelp = $("#template-menu-help")
    const notesmenuHelp = $("#notes-menu-help")
    const combatHelp = $("#combat-tracker-help")
    
    let fadeincheck = false;
    let fadeoutcheck = false;
    let fadeincheckchat = false;
    let fadeoutcheckchat = false;
    
    chatboxHelp.hide()
    macrobarHelp.hide()
    combatHelp.hide()
    
if (game.settings.get("butler", 'hide-chat-box-help') === false) {
  $("#chat-form").mouseenter(function(){
     if (fadeincheckchat === false) {
      fadeincheckchat = true;
      chatboxHelp.delay(300).fadeIn("slow")
      setTimeout(function() {fadeincheckchat = false},300)
    }   
  })
    $("#chat-form").mouseleave(function(){
      if (fadeoutcheckchat === false) {
        fadeoutcheckchat = true
        chatboxHelp.delay(50).fadeOut();
        setTimeout(function() {fadeoutcheckchat = false},300)
    }
  }) 
}
  if (game.settings.get("butler", 'hide-macro-bar-help') === false) {
 $("#hotbar.flexrow").mouseenter(function(){
    if (fadeincheck === false) {
    fadeincheck = true;
     
     macrobarHelp.delay(300).fadeIn("slow")
    
     setTimeout(function() {fadeincheck = false},300)
   }   
 })
 
 $("#hotbar.flexrow").mouseleave(function(){
     if (fadeoutcheck === false) {
       fadeoutcheck = true
       macrobarHelp.delay(50).fadeOut();
       setTimeout(function() {fadeoutcheck = false},300)
   }
 })
}

if (game.settings.get("butler", 'hide-sidebar-help') === false) {
 $("#sidebar").mouseenter(function(){
  if (fadeincheck === false) {
    fadeincheck = true;
     
     sidebartabHelp.delay(300).fadeIn("slow")
    
     setTimeout(function() {fadeincheck = false},300)
   }
   
   $("#sidebar").mouseleave(function(){
    if (fadeoutcheck === false) {
      fadeoutcheck = true
      sidebartabHelp.delay(50).fadeOut();
      setTimeout(function() {fadeoutcheck = false},300)
    }
    }) 
 
  })
  }
}

}