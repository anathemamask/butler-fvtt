import constants from "./constants.js";
export default function registerSettings() {
  game.settings.register("butler", "hide-all", {
    name: "Hide all butler overlays",
    hint: "Disables all hints from Butler",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  game.settings.register("butler", "hide-macro-bar-help", {
    name: "Hide Macro-Bar Help",
    hint: "Hides the on-hover hints on the macro bar",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  game.settings.register("butler", "hide-chat-box-help", {
    name: "Hide Chat-box Help",
    hint: "Hides the on-hover hints for the chat text-entry field",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  game.settings.register("butler", "hide-toolbar-help", {
    name: "Hide all toolbar help",
    hint: "Disables all toolbar help",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  game.settings.register("butler", "hide-token-help-frame", {
    name: "Hide all token help",
    hint: "Disables all token help overlays",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  game.settings.register("butler", "hide-token-move-help", {
    name: "Hide token movement tips",
    hint: "Hides token movement controls help text",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
    
  game.settings.register("butler", "hide-token-click-help", {
    name: "Hide token click help",
    hint: "Hides tips for clicking or right clicking a token",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  game.settings.register("butler", "hide-token-hud-help", {
    name: "Hide token hud help",
    hint: "Hides help text for the token HUD",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  game.settings.register("butler", "hide-sidebar-help", {
    name: "Hide sidebar help",
    hint: "Hides help text for sidebar tabs",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: value => {
      window.location.reload();
    }
  });
  
  
  
}