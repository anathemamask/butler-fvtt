# Butler UX Suite

A personal guide to using Foundry VTT, intended to help new users by providing highlighted UI hints and tips in an unobtrusive way.
Configuration options allow users to disable particular types of overlay hints.

No self-respecting user should be without their Butler.

## Features
Provides a UI overlay which displays helpful text hints for users in order to showcase functions they might not be aware of, or remind them what certain options do.

![](./Butler-Preview.mp4)
Currently supported:

**Player UI Overlay**
 - Macro Bar hints
 - Sidebar hints
 - Chat entry hints
 - Rollmode hints
 - Token Tools help text
 - Measurement Tools help text
 - Map Note Tools help text
 - Token movement and control hints (100px tokens)
 
 **GM and Dev Overlay not yet implemented**
 
## Roadmap
Butler will be a constantly evolving project, expanding on its features to provide increasing levels of assistance not only at the player level, but at the GM and Dev level as well.

Planned Features
 - Introductory screen that informs users exactly once that they can change which tips are revealed via the settings menu
 
**Player UI Overlay**
 - Drawing tools help text
 - Expanded Sidebar hints for each sidebar tab when selected
 - Token configuration window hints
 
**GM UI Overlay**
 - Tile tools help text
 - Drawing tools help text
 - Wall tools help text
 - Light tools help text
 - Ambient sound tools help text
 - Wall layer tips
 - Lighting layer tips
 - Drawing layer tips
 - Configuration window tips for walls, lights, sounds, drawings
 - Rollable Table window tips
 - Configure Player menu tips
 - Permissions screen tips
 - Settings window tips
 - Sidebar tips for GM specifically

**Dev UI Overlay** 
 - Object path overlay and/or html ID/Class on hover
 - Selectable _ID field appended to all objects that have an ID
 - "Log context" button on actors and items to fire a console.log of the object
 
## Known Issues
This module will very likely not play well with:

 - Any other module that modifies the UI (results may vary.) 	
 - The WFRP system (Sorry Moo Man!)
 - Screens that do not meet the FVTT minimum screen size requirement
 - Selection of tokens larger or smaller than 100px (at this time.)
 - *Firefox (probably)*

## License
Open. Do whatever you want with this. I did things to the javascript and css for this project that would shame the devil.
